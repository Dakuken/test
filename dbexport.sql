-- MySQL dump 10.13  Distrib 8.0.31, for Linux (x86_64)
--
-- Host: localhost    Database: QUESTIONS
-- ------------------------------------------------------
-- Server version	8.0.31

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `Question`
--

DROP TABLE IF EXISTS `Question`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Question` (
  `id_theme` int NOT NULL,
  `id_question` int NOT NULL AUTO_INCREMENT,
  `labelle` varchar(250) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `url` varchar(250) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `difficulte` int NOT NULL,
  `reponse_index` varchar(5) NOT NULL,
  PRIMARY KEY (`id_question`),
  KEY `id_theme` (`id_theme`)
) ENGINE=InnoDB AUTO_INCREMENT=52 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Question`
--

LOCK TABLES `Question` WRITE;
/*!40000 ALTER TABLE `Question` DISABLE KEYS */;
INSERT INTO `Question` VALUES (1,1,'Qui a gagné la coupe du monde 1998 ?','https://firebasestorage.googleapis.com/v0/b/tierlist-portfolio.appspot.com/o/FOOT%2F1.jpg?alt=media&token=550d4824-5e92-4ad7-9698-f2269bb147b8',1,'0'),(1,2,'Combien de ballon d\'or à Messi','https://firebasestorage.googleapis.com/v0/b/tierlist-portfolio.appspot.com/o/FOOT%2F2.webp?alt=media&token=df476b44-43ae-44f7-b767-fbc1483b3dc1',1,'1'),(1,3,'Qui a marqué le plus de but en une coupe du monde','https://firebasestorage.googleapis.com/v0/b/tierlist-portfolio.appspot.com/o/FOOT%2F3.jpg?alt=media&token=d1163c91-2f35-4523-b0c0-d068d263d214',1,'2'),(1,4,'Combien de trophé possède la Belgique en 2022','https://firebasestorage.googleapis.com/v0/b/tierlist-portfolio.appspot.com/o/FOOT%2F4.jpg?alt=media&token=6f39b52c-b087-4014-9616-39b57046b0c6',1,'3'),(1,5,'Quel objet les capitaines s’échangent-ils avant un match international ?','https://firebasestorage.googleapis.com/v0/b/tierlist-portfolio.appspot.com/o/FOOT%2F5.webp?alt=media&token=fa9cbc28-eb8a-4d2a-b994-d46863dac88a',1,'0'),(1,6,'Quel a été le premier président de la République à remettre le trophée de la Coupe de France ?','https://firebasestorage.googleapis.com/v0/b/tierlist-portfolio.appspot.com/o/FOOT%2F6.webp?alt=media&token=560eb143-fffa-4e05-9f7d-cf6828fb4778',1,'1'),(1,7,'Combien y a-t-il de clubs amateurs de football en France ?','https://firebasestorage.googleapis.com/v0/b/tierlist-portfolio.appspot.com/o/FOOT%2F7.jpg?alt=media&token=16c336ee-e48a-4856-8fc5-0948bbdd2f79',1,'2'),(1,8,'Qui était sélectionneur(e) de l’Équipe de France féminine lors de la Coupe du monde 2019 ?','https://firebasestorage.googleapis.com/v0/b/tierlist-portfolio.appspot.com/o/FOOT%2F8.jpg?alt=media&token=820ab07a-7f1a-4156-80b7-a444d8873306',1,'3'),(1,9,'Quel objet les arbitres utilisent-ils pour déterminer le placement des joueurs lorsqu’ils doivent former un mur ?','https://firebasestorage.googleapis.com/v0/b/tierlist-portfolio.appspot.com/o/FOOT%2F9.jpg?alt=media&token=9707369e-24c0-49af-9c66-60a21618b479',1,'0'),(1,10,'Qui fut le premier président de la FFF','https://firebasestorage.googleapis.com/v0/b/tierlist-portfolio.appspot.com/o/FOOT%2F10.webp?alt=media&token=d046070e-d5ba-4bbf-8fb6-a9082d7f5a3e',1,'1'),(1,11,'Que désigne le terme « kop » dans un stade ?','https://firebasestorage.googleapis.com/v0/b/tierlist-portfolio.appspot.com/o/FOOT%2F11.jpg?alt=media&token=4bcae9be-3ad2-4973-aef8-41a52e798f4e',1,'0'),(1,12,'Combien de buts a marqué Pelé en carrière ?','https://firebasestorage.googleapis.com/v0/b/tierlist-portfolio.appspot.com/o/FOOT%2F12.webp?alt=media&token=19806207-bdad-478e-92d0-8ef8f15c13d4',1,'1'),(1,13,'Quel est le symbole de la Coupe du Monde ?','https://firebasestorage.googleapis.com/v0/b/tierlist-portfolio.appspot.com/o/FOOT%2F13.jpg?alt=media&token=a3ead6d0-717f-4e80-972a-778c5b365994',1,'2'),(1,14,'Quel est le nom du trophée de la Ligue des Champions ?','https://firebasestorage.googleapis.com/v0/b/tierlist-portfolio.appspot.com/o/FOOT%2F14.jpg?alt=media&token=92c088b7-1b0e-4863-8cd5-0684def140d3',1,'3'),(1,15,'Quel est le nom du trophée de la Ligue Europa ?','https://firebasestorage.googleapis.com/v0/b/tierlist-portfolio.appspot.com/o/FOOT%2F15.jpg?alt=media&token=d67c4abd-726b-4fe6-9e6d-65c8b51ef319',1,'0'),(2,21,'Combien de film comporte la saga jurassic park ?','https://firebasestorage.googleapis.com/v0/b/tierlist-portfolio.appspot.com/o/FILM%2F21.png?alt=media&token=e2622037-8f86-49b3-9676-31e4cc43c72b',1,'2'),(2,22,'En quelle année est sortie la première star Wars ?','https://firebasestorage.googleapis.com/v0/b/tierlist-portfolio.appspot.com/o/FILM%2F22.png?alt=media&token=f605df8b-da53-4e18-8ca4-af6cfc550c5c',1,'0'),(2,23,'Quel film a reçu l\'oscar du meilleur film en 2017 ?','https://firebasestorage.googleapis.com/v0/b/tierlist-portfolio.appspot.com/o/FILM%2F23.jpg?alt=media&token=2dba31bd-6a67-48d5-902b-9f54bcdfb505',1,'3'),(2,24,'Quel film est le plus ancien ?','https://firebasestorage.googleapis.com/v0/b/tierlist-portfolio.appspot.com/o/FILM%2F24.jpg?alt=media&token=ddb75250-f715-47a8-9ec8-e413755df378',1,'3'),(2,25,'D\'après certaines traditions, quelle serait la seule manière de tuer un loup-garou ?','https://firebasestorage.googleapis.com/v0/b/tierlist-portfolio.appspot.com/o/FILM%2F25.jpg?alt=media&token=0119edb7-4a77-4218-8df9-b4946b180e4a',1,'2'),(2,26,'Quelle est la véritable identité de Batman ?','https://firebasestorage.googleapis.com/v0/b/tierlist-portfolio.appspot.com/o/FILM%2F26.jpg?alt=media&token=adf6a9bd-9bcd-48d4-8ae1-dba199944ad8',1,'0'),(2,27,'Quel est le scénariste qui a créé le personnage de Spider-Man ?','https://firebasestorage.googleapis.com/v0/b/tierlist-portfolio.appspot.com/o/FILM%2F27.jpg?alt=media&token=faee879b-8587-4271-a648-3f2eb04036fe',1,'3'),(2,28,'Quelle est la formule du sortilège de mort dans la saga Harry Potter ?','https://firebasestorage.googleapis.com/v0/b/tierlist-portfolio.appspot.com/o/FILM%2F28.jpg?alt=media&token=3074b5eb-450a-4e07-b8c9-2c77c2e83885',1,'0'),(2,29,'Quel Oscar n\'a pas été remporté par Jurassic Park ? ','https://firebasestorage.googleapis.com/v0/b/tierlist-portfolio.appspot.com/o/FILM%2F29.jpg?alt=media&token=ea39d916-77c2-47f4-ae0a-85b33fea326e',1,'3'),(2,30,'Quel acteur n ’ a jamais incarné Batman au cinéma ?','https://firebasestorage.googleapis.com/v0/b/tierlist-portfolio.appspot.com/o/FILM%2F30.jpg?alt=media&token=cc2b15ce-c98b-4654-9fcb-87266e752be4',1,'2'),(2,31,'En quelle année le dessin animé Le Roi Lion est - il sorti au cinéma ?','https://firebasestorage.googleapis.com/v0/b/tierlist-portfolio.appspot.com/o/FILM%2F31.jpg?alt=media&token=70b81665-f3b6-4f8f-923d-44223a23a8dc',1,'2'),(2,32,'Quelle information est fausse concernant Le Génie dans le dessin animé Aladdin de Disney ?','https://firebasestorage.googleapis.com/v0/b/tierlist-portfolio.appspot.com/o/FILM%2F32.jpg?alt=media&token=670f938b-a2d1-4697-965e-3805ac9fb048',1,'1'),(2,33,'Dans quel film le méchant porte le nom de Grippe-Sou ?','https://firebasestorage.googleapis.com/v0/b/tierlist-portfolio.appspot.com/o/FILM%2F33.jpg?alt=media&token=5e4ba1b7-80b2-45b6-84df-822fa1320eea',1,'2'),(2,34,'En quelle année est sorti au cinéma le dernier film de la saga Harry Potter, la seconde partie des Reliques de la Mort ?','https://firebasestorage.googleapis.com/v0/b/tierlist-portfolio.appspot.com/o/FILM%2F34.jpg?alt=media&token=5c0d90b2-fde3-41ac-ad20-73136b50728c',1,'3'),(2,35,'Qui est le premier personnage Marvel à avoir endossé le symbiote Venom ?','https://firebasestorage.googleapis.com/v0/b/tierlist-portfolio.appspot.com/o/FILM%2F35.jpg?alt=media&token=859a707c-55eb-4883-9b60-d20c489b5574',1,'0'),(2,36,' Qui est la chanteuse qui interprète Libérée,\n        Délivrée dans le film Disney La Reine des Neiges ? ','https://firebasestorage.googleapis.com/v0/b/tierlist-portfolio.appspot.com/o/FILM%2F36.jpg?alt=media&token=9d003333-5064-4d55-8957-139b6ed50d4a',1,'1'),(2,37,'Dans jurassic park quel est le nom du propriétaire du parc dans la première trilogie ?','https://firebasestorage.googleapis.com/v0/b/tierlist-portfolio.appspot.com/o/FILM%2F37.jpg?alt=media&token=1a1a6242-be70-45cd-9f31-9124ac59a7ba',1,'2'),(2,38,' Qui permet à Spider - Man d ’ être ressuscité dans Avengers Endgame ? ','https://firebasestorage.googleapis.com/v0/b/tierlist-portfolio.appspot.com/o/FILM%2F38.jpg?alt=media&token=eecc0cde-a88d-49e1-967f-12e3be2d0f3b',1,'3'),(2,39,' Quelle actrice est devenue célèbre grâce à la saga de films d\'horreur Halloween ?','https://firebasestorage.googleapis.com/v0/b/tierlist-portfolio.appspot.com/o/FILM%2F39.jpg?alt=media&token=a7afb8f6-5de8-4ff7-a8bb-e2d299b90c73',1,'1'),(2,40,'Dans retour vers le futur pour voyager dans le temps,\n        quel objet est utilisé ?','https://firebasestorage.googleapis.com/v0/b/tierlist-portfolio.appspot.com/o/FILM%2F40.png?alt=media&token=f1776b6b-96d7-43ad-83d0-fd4431c8df56',1,'3'),(1,41,'De quelle marque est la 2 chevaux ?','https://firebasestorage.googleapis.com/v0/b/tierlist-portfolio.appspot.com/o/AUTOMOBILE%2F41.png?alt=media&token=78974432-bed0-426a-95dd-58decd459868',1,'2'),(1,42,'Quel brevet de mécanique acheté par André Citroën est à l’origine de la marque « aux chevons » ?','https://firebasestorage.googleapis.com/v0/b/tierlist-portfolio.appspot.com/o/AUTOMOBILE%2F42.png?alt=media&token=b2a32c76-dd28-422f-b91b-b2654c7459b1',1,'3'),(1,43,'Quel modèle de Renault a été surnommé la «motte de beurre» ?','https://firebasestorage.googleapis.com/v0/b/tierlist-portfolio.appspot.com/o/AUTOMOBILE%2F43.png?alt=media&token=ca39efe3-6e82-42b5-abf6-cf21e4623856',1,'0'),(1,44,'Surnommée «Käfer» en Allemagne ou «Beetle» au Royaume-Uni, c’est','https://firebasestorage.googleapis.com/v0/b/tierlist-portfolio.appspot.com/o/AUTOMOBILE%2F44.png?alt=media&token=23f3ce00-b668-4199-904e-72b177a9a9d8',1,'3'),(1,45,'En quelle année la Citroën 2 CV est présentée au Salon de l’auto ?','https://firebasestorage.googleapis.com/v0/b/tierlist-portfolio.appspot.com/o/AUTOMOBILE%2F45.jpg?alt=media&token=9ac50f8f-6e29-48f2-8c96-2acab2e1f1bf',1,'2'),(1,46,'La Renault Captur, la Nissan Qashqai, le Tiguan de Volkswagen sont des SUV. SUV signifie ?','https://firebasestorage.googleapis.com/v0/b/tierlist-portfolio.appspot.com/o/AUTOMOBILE%2F46.png?alt=media&token=6da4c999-d7de-4b0f-9645-816ee646e82e',1,'0'),(1,47,'Al Capone conduisait une','https://firebasestorage.googleapis.com/v0/b/tierlist-portfolio.appspot.com/o/AUTOMOBILE%2F47.png?alt=media&token=0f840508-441a-4b04-b2d5-0773744827af',1,'1'),(1,48,'Quelle voiture est la plus vendue au monde depuis les débuts de l’automobile ?','https://firebasestorage.googleapis.com/v0/b/tierlist-portfolio.appspot.com/o/AUTOMOBILE%2F48.png?alt=media&token=35d9ef55-9cfe-4e54-a213-b20c4a8ead90',1,'2'),(1,49,'Quelle marque est la première à offrir en série les ceintures de sécurité à l’avant ?','https://firebasestorage.googleapis.com/v0/b/tierlist-portfolio.appspot.com/o/AUTOMOBILE%2F49.png?alt=media&token=8453131f-57b7-4530-a2a7-6879386fbd62',1,'0'),(1,50,'De quel pays vient la culture du drift','https://firebasestorage.googleapis.com/v0/b/tierlist-portfolio.appspot.com/o/AUTOMOBILE%2F50.png?alt=media&token=fc94fdf6-b091-404f-9cb3-1e880d842afb',1,'1'),(1,51,'Quelle puissance avait la première 205 GTI ?','https://firebasestorage.googleapis.com/v0/b/tierlist-portfolio.appspot.com/o/AUTOMOBILE%2F51.png?alt=media&token=81e7b10c-1a68-4376-a086-ef797950db37',1,'0');
/*!40000 ALTER TABLE `Question` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Reponse`
--

DROP TABLE IF EXISTS `Reponse`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Reponse` (
  `id` int NOT NULL,
  `numero` int NOT NULL,
  `labelle` varchar(50) NOT NULL,
  PRIMARY KEY (`id`,`numero`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Reponse`
--

LOCK TABLES `Reponse` WRITE;
/*!40000 ALTER TABLE `Reponse` DISABLE KEYS */;
INSERT INTO `Reponse` VALUES (1,0,'FRANCE'),(1,1,'ESPAGNE'),(1,2,'ANGLETERRE'),(1,3,'BRESIL'),(2,0,'6'),(2,1,'7'),(2,2,'5'),(2,3,'4'),(3,0,'Messi'),(3,1,'Ronaldo'),(3,2,'Just Fontaine'),(3,3,'Mbappe'),(4,0,'3'),(4,1,'2'),(4,2,'1'),(4,3,'0'),(5,0,'Leurs fanions'),(5,1,'Leurs maillots'),(5,2,'Leurs brassards'),(5,3,'Leurs drapeaux'),(6,0,'Charles de Gaulle'),(6,1,'Gaston Doumergue'),(6,2,'Emmanuel Macron'),(6,3,'Nicolas Sarkozy'),(7,0,'25 000 - 30 000'),(7,1,'20 000 - 25 000'),(7,2,'10 000 - 20 000'),(7,3,'5 000 - 10 000'),(8,0,'Didier Deschamps'),(8,1,'Wendie Renard'),(8,2,'Eve Perisset'),(8,3,'Corinne Diacre'),(9,0,'Un spray'),(9,1,'Une corde'),(9,2,'Un bâton'),(9,3,'Une lumière'),(10,0,'Didier Deschamps'),(10,1,'Jules Rimet'),(10,2,'Zinedine Zidane'),(10,3,'Fernand Sastre'),(11,0,'Une tribune'),(11,1,'Un terrain'),(11,2,'Un ballon'),(11,3,'Un maillot'),(12,0,'281'),(12,1,'1 281'),(12,2,'2 281'),(12,3,'3 281'),(13,0,'Un ballon'),(13,1,'Un maillot'),(13,2,'Un trophée'),(13,3,'Un stade'),(14,0,'La Coupe de l’Europe des Nations'),(14,1,'La Coupe de l’Europe'),(14,2,'La Coupe de UEFA'),(14,3,'Coupe des clubs champions européens'),(15,0,'La Coupe de l’UEFA'),(15,1,'La Coupe de l’Europe des Clubs'),(15,2,'La Coupe de l’Europe'),(15,3,'La Coupe de l’Europe des Nations'),(21,0,'3'),(21,1,'2'),(21,2,'6'),(21,3,'5'),(22,0,'1977'),(22,1,'1980'),(22,2,'1975'),(22,3,'1976'),(23,0,'Spider-Man: Homecoming'),(23,1,'La Planète des singes : Suprématie'),(23,2,'Transformers: The Last Knight'),(23,3,'Moonlight'),(24,0,'Lucifer Rising'),(24,1,'Bigfoot Lives'),(24,2,'Othello'),(24,3,'L\'Émeraude tragique'),(25,0,'La lumière du jour'),(25,1,'Une croix satanique'),(25,2,'Une balle en argent'),(25,3,'Le sang d\'une vierge'),(26,0,'Bruce Wayne'),(26,1,'Bruce Yaine'),(26,2,'Bruce Waine'),(26,3,'Bruce Yane'),(27,0,'Larry Lieber'),(27,1,'Martin Goodman'),(27,2,'Jack Kirby'),(27,3,'Stan lee'),(28,0,'Avada Kedavra'),(28,1,'Mortibus Rem'),(28,2,'Machina Fino'),(28,3,'Post Mortem'),(29,0,'Oscar du meilleur son'),(29,1,'Oscar du meuilleur montage sonore'),(29,2,'Oscar des meuilleurs effets visuels'),(29,3,'Oscar du meuilleur film'),(30,0,'Goerges clooney'),(30,1,'Michael Keaton'),(30,2,'Matt Damon'),(30,3,'Robert Pattinson'),(31,0,'1990'),(31,1,'1992'),(31,2,'1994'),(31,3,'1996'),(32,0,'Il possède une boucle d\'oreille '),(32,1,'Il a des mains à 5 doigts'),(32,2,'Il porte une ceinture rouge'),(32,3,'Il porte une barbichette'),(33,0,'Chucky'),(33,1,'Aquaman'),(33,2,'Ça'),(33,3,'Harry Potter'),(34,0,'2013'),(34,1,'2009'),(34,2,'2015'),(34,3,'2011'),(35,0,'Peter Parker'),(35,1,'Eddie Brock'),(35,2,'Le bouffon vert'),(35,3,'Cletus Kasady'),(36,0,'Alizé'),(36,1,'Anaïs Delva'),(36,2,'Claire Kleim'),(36,3,'Julie Zénatti'),(37,0,'Alan Grant'),(37,1,'Ian Malcolm'),(37,2,'John Hammond'),(37,3,'Dennis Nedry'),(38,0,'Iron Man'),(38,1,'Deadpool'),(38,2,'Captain America'),(38,3,'Hulk'),(39,0,'Jane Fonda'),(39,1,'Jamie Lee Curtis'),(39,2,'Sigourney Weaver'),(39,3,'Winona Ryder'),(40,0,'Un bracelet'),(40,1,'Une porte temporelle'),(40,2,'Un frigo'),(40,3,'Une voiture'),(41,0,'Volkswagen'),(41,1,'Renault'),(41,2,'Citroën'),(41,3,'Peugeot'),(42,0,'Le cardan'),(42,1,'La papaille'),(42,2,'Le double piston'),(42,3,'La fabrication d’engrenages'),(43,0,'La 4 CV'),(43,1,'La 4 L'),(43,2,'La DS'),(43,3,'La 2 CV'),(44,0,'La 2 CV'),(44,1,'La Fiat 500'),(44,2,'La 4 L'),(44,3,'La Coccinelle'),(45,0,'En 1938'),(45,1,'En 1958'),(45,2,'En 1948'),(45,3,'En 1968'),(46,0,'Sport Utility Vehicle'),(46,1,'Special Urban Van'),(46,2,'Super Up-and-down Vibrations'),(46,3,'Ce n\' est pas un acronyme '),(47,0,' Ford T '),(47,1,' Cadillac Sedan Town '),(47,2,' Bugatti Royale '),(47,3,' Le 1000tiplat '),(48,0,' La Golf de Volkswagen '),(48,1,' La Ford Fiesta '),(48,2,' La Corolla de Toyota '),(48,3,' La Renault Clio '),(49,0,' Volvo en 1959 '),(49,1,' Peugeot en 1925 '),(49,2,' Seat en 1971 '),(49,3,' Volkswagen en 1937 '),(50,0,' Des USA '),(50,1,' Du Japon '),(50,2,' De Corée '),(50,3,'D\' Allemagne '),(51,0,'105cv'),(51,1,'115cv'),(51,2,'122cv'),(51,3,'130cv');
/*!40000 ALTER TABLE `Reponse` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Stat`
--

DROP TABLE IF EXISTS `Stat`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Stat` (
  `id` int NOT NULL,
  `juste` int NOT NULL,
  `faux` int NOT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `Stat_ibfk_1` FOREIGN KEY (`id`) REFERENCES `Question` (`id_question`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Stat`
--

LOCK TABLES `Stat` WRITE;
/*!40000 ALTER TABLE `Stat` DISABLE KEYS */;
INSERT INTO `Stat` VALUES (1,1,0),(2,1,0),(3,2,2),(4,6,0),(5,3,0),(6,0,5),(7,1,3),(8,0,3),(10,0,0),(11,5,1),(12,1,0),(13,0,0),(14,1,0),(15,0,0),(21,0,0),(22,0,0),(23,1,0),(24,0,1),(25,5,0),(26,0,0),(27,1,4),(29,2,2),(30,0,0),(31,0,1),(32,10,2),(33,2,1),(35,2,3),(36,1,0),(37,1,3),(39,2,0),(40,2,1),(41,4,1),(42,0,0),(44,4,0),(45,0,0),(46,0,0),(48,3,2),(49,1,0),(50,7,0),(51,0,1);
/*!40000 ALTER TABLE `Stat` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Theme`
--

DROP TABLE IF EXISTS `Theme`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Theme` (
  `id_theme` int NOT NULL AUTO_INCREMENT,
  `labelle` varchar(50) NOT NULL,
  PRIMARY KEY (`id_theme`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Theme`
--

LOCK TABLES `Theme` WRITE;
/*!40000 ALTER TABLE `Theme` DISABLE KEYS */;
INSERT INTO `Theme` VALUES (1,'FOOT'),(2,'FILM');
/*!40000 ALTER TABLE `Theme` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2023-01-16  4:42:14
